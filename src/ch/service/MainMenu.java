package ch.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class MainMenu {
    private final List<Integer> Data_Nilai = new ArrayList<>();

    public void tampil(){
        Scanner input = new Scanner(System.in);
        System.out.println("============================");
        System.out.println("Aplikasi Pengolah Nilai Siswa");
        System.out.println("============================");

        System.out.println("Letakkan file data_sekolah.csv difolder yang sama dengan aplikasi ini.");
        System.out.println("Jika sudah, tekan ENTER");
        input.nextLine();
        ReadWrite readWrite = new ReadWrite();

        System.out.println("Output akan di generate ke folder \"Output\" yang baru saja dibuat");
        System.out.println("1. Generate File untuk menampilkan mean, median, dan modus");
        System.out.println("2. Generate File untuk menampilkan pengelmompokan data");
        System.out.println("0. Exit");
        System.out.print("Silahkan pilih menu: ");
        int pilih = input.nextInt();
        switch(pilih){
            case 1 -> {
                readWrite.generateOperation("txtOperation");
            }
            case 2 -> {
                System.out.println("Masukkan Nilai Pembatas : ");
                int delimiter = input.nextInt();
                readWrite.generateGroup("txtGrouping", delimiter);
            }
            case 0 -> {
                System.exit(0);
            }
        }


    }
}

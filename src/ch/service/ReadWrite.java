package ch.service;

import ch.model.Group;
import ch.model.operation;

import java.io.*;
import java.util.*;


public class ReadWrite {

    public final List<Integer> Data_Nilai = new ArrayList<>();
    private final String HASIL = "FinalFile";
    private final Integer delimiter = 8;

    ReadWrite() {
        File createDirectory = new File(HASIL);
        if (createDirectory.mkdir()) {
            System.out.println(HASIL + "berhasil di cetak");
        }

        String fileName = "data_sekolah.csv";

        List<List<String>> data = this.readFIle(fileName, ";");

        for (List<String> dataTiapKelas : data) {
            dataTiapKelas.remove(0);
            for (int j = 0; j < dataTiapKelas.size(); j++) {
                Data_Nilai.add(Integer.valueOf(dataTiapKelas.get(j)));
            }
        }
        Collections.sort(Data_Nilai);

//        kelompokNilai();
//        generateOperation("txtMedian", nilaiMedian, nilaiMode, nilaiMean);
//        generateGroup();
    }

//    public void kelompokNilai(Data_Nilai,int delimiter){
//        Group.kelompok(Data_Nilai, delimiter);
//    }

    public void generateOperation(String txtFile){
        operation oper = new operation(Data_Nilai);
        try {
            File file = new File(txtFile);
            if (file.createNewFile()){
                System.out.println("New file is created");
            }
            FileWriter writer = new FileWriter(file);
            BufferedWriter bwr = new BufferedWriter(writer);
            bwr.write("Nilai Mean\t\t: " + oper.getMean());
            bwr.newLine();
            bwr.write("Nilai Median\t: " + oper.getMedian());
            bwr.newLine();
            bwr.write("Nilai Mode\t\t: " + oper.getMode());
            bwr.newLine();
            bwr.flush();
            bwr.close();
            System.out.println("File succesfully written");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void generateGroup(String txtFile, int delimiter){
        Group grup = new Group(Data_Nilai, delimiter);
        try {
            File file = new File(txtFile);
            if (file.createNewFile()){
                System.out.println("New file is created");
            }
            FileWriter writer = new FileWriter(file);
            BufferedWriter bwr = new BufferedWriter(writer);

            /**
             * Nilai Kurang Dari Delimiter
             */
            HashMap<Integer, Integer> kurang = new HashMap<>(grup.getNilaiKurangDari());
            for (Integer nilai2 : kurang.keySet()) {
                bwr.write("Nilai " + nilai2 + " berjumlah " + kurang.get(nilai2));
                bwr.newLine();
            }
            bwr.newLine();

            /**
             * Nilai Sama Dengan Delimiter
             */
            HashMap<Integer, Integer> tengah = new HashMap<>(grup.getNilaiTengah());
            for (Integer nilai3 : tengah.keySet()) {
                bwr.write("Nilai " + nilai3 + " berjumlah " + tengah.get(nilai3));
                bwr.newLine();
            }
            bwr.newLine();

            /**
             * Nilai Lebih Dari Delimiter
             */
            HashMap<Integer, Integer> jumlah = new HashMap<>(grup.getNilaiLebihDari());
            for (Integer nilai : jumlah.keySet()) {
                bwr.write("Nilai " + nilai + " berjumlah " + jumlah.get(nilai));
                bwr.newLine();
            }
            bwr.newLine();

            bwr.newLine();
            bwr.flush();
            bwr.close();
            System.out.println("File succesfully written");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private List<List<String>> readFIle(String fileName, String delimiter){
       try{
           File file = new File(fileName);
           FileReader fr = new FileReader(file);
           BufferedReader br = new BufferedReader(fr);
           String line;
           String[] tempArr;
           List<String>innerList;
           List<List<String>> outerList = new ArrayList<>();
           while ((line=br.readLine())!=null){
                tempArr = line.split(delimiter);
                innerList = new ArrayList<>(Arrays.asList(tempArr));
                outerList.add(innerList);
           }
           br.close();
            return outerList;
       }catch (Exception e){
           e.printStackTrace();
           return null;
       }
     }
}

package ch.model;

import java.util.List;

public class operation {

    private double mean;
    private double median;
    private int mode;

    public operation(List<Integer> data){
        this.htgMean(data);
        this.htgMedian(data);
        this.htgMode(data);
    }

    public double getMean(){
        return mean;
    }
    public int getMode(){
        return mode;
    }
    public double getMedian(){
        return median;
    }

    public void htgMean(List<Integer> m) {
        double sum = 0;
        for (int i = 0; i < m.size(); i++) {
            sum += m.get(i);
        }
        double rata = sum / m.size();
        this.mean = rata;
    }


    public void htgMode(List<Integer> m) {
        int n = m.size();
        int maxValue = 0, maxCount = 0, i, j;

        for (i = 0; i < n; ++i) {
            int count = 0;
            for (j = 0; j < n; ++j) {
                if (m.get(j) == m.get(i))
                    ++count;
            }

            if (count > maxCount) {
                maxCount = count;
                maxValue = m.get(i);
            }
        }
        this.mode = maxValue;
    }

    public void htgMedian(List<Integer> m) {
        double nTengah;
        if (m.size() % 2 == 1)
            nTengah = m.get(m.size() + 1 / 2-1);
        else
            nTengah = ((double) (m.get(m.size() / 2-1) + m.get(m.size() / 2))) / 2;
        this.median = nTengah;
    }

}

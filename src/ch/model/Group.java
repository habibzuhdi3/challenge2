package ch.model;

import java.util.HashMap;
import java.util.List;
import java.util.Objects;

public class Group {
    private HashMap<Integer, Integer> grupLebihDari = new HashMap<>();
    private HashMap<Integer, Integer> grupKurangDari = new HashMap<>();
    private HashMap<Integer, Integer> grupTengah = new HashMap<>();
    public Group(List<Integer> data, int delimiter){
        this.setNilaiLebihDari(data, delimiter);
        this.setNilaiKurangDari(data, delimiter);
        this.setNilaiTengah(data,delimiter);
    }
    public HashMap<Integer, Integer> getNilaiLebihDari(){return grupLebihDari;}
    public HashMap<Integer, Integer> getNilaiKurangDari(){return grupKurangDari;}
    public HashMap<Integer, Integer> getNilaiTengah(){return grupTengah;}
    public void setNilaiLebihDari(List<Integer> data, int delimiter){
        HashMap<Integer, Integer> map = new HashMap<>();

        for (Integer nilai : data) {
            int count = 0;
            if(nilai > delimiter) {
                for (Integer nilai2 : data) {
                    if(Objects.equals(nilai2, nilai)) {
                        ++count;
                    }
                }
                this.grupLebihDari.put(nilai, count);
            }
        }
    }

    public void setNilaiKurangDari(List<Integer> data, int delimiter){
        HashMap<Integer, Integer> map = new HashMap<>();

        for (Integer nilai : data) {
            int count = 0;
            if(nilai < delimiter) {
                for (Integer nilai2 : data) {
                    if(Objects.equals(nilai2, nilai)) {
                        ++count;
                    }
                }
                this.grupKurangDari.put(nilai, count);
            }
        }
    }

    public void setNilaiTengah(List<Integer> data, int delimiter){
        HashMap<Integer, Integer> map = new HashMap<>();

        for (Integer nilai : data) {
            int count = 0;
            if(nilai == delimiter) {
                for (Integer nilai2 : data) {
                    if(Objects.equals(nilai2, nilai)) {
                        ++count;
                    }
                }
                this.grupTengah.put(nilai, count);
            }
        }
    }
    public static void kelompok(List<Integer> m, int limiter) {
        int n = m.size();
        int i, j;

        HashMap<Integer, Integer> map = new HashMap<>();

        for (i = 0; i < n; ++i) {
            int count = 0;
            for (j = 0; j < n; ++j) {
                if (m.get(j) == (m.get(i))){
                    ++count;
                    map.put(m.get(j),count);
                }
            }
        }
//        System.out.println(map.values());
//        for (var angka : map.keySet()) {
//            if (angka < delimiter){
//                System.out.println("Nilai Lebih Kecil dari\t" + delimiter + ". Nilai " + angka + " berjumlah " + map.get(angka));
//            } else if(angka > delimiter) {
//                System.out.println("Nilai Lebih Besar dari\t" + delimiter + ". Nilai " + angka + " berjumlah " + map.get(angka));
//            } else {
//                System.out.println("Nilai Pembatas adalah\t" + delimiter + ". Nilai " + angka + " berjumlah " + map.get(angka));
//            }
//        }
    }
//    public  void groupKurangDari(List<Integer> m, int delimiter) {
//        int n = m.size();
//        int i, j;
//
//        HashMap<Integer, Integer> map = new HashMap<>();
//
//        for (i = 0; i < n; ++i) {
//            int count = 0;
//            for (j = 0; j < n; ++j) {
//                if (m.get(j) == (m.get(i))){
//                    ++count;
//                    map.put(m.get(j),count);
//                }
//            }
//        }
//        System.out.println(map.values());
//        for (var angka : map.keySet()) {
//            if (angka < delimiter){
//                System.out.println("Nilai Lebih Kecil dari\t" + delimiter + ". Nilai " + angka + " berjumlah " + map.get(angka));
//            } else if(angka > delimiter) {
//                System.out.println("Nilai Lebih Besar dari\t" + delimiter + ". Nilai " + angka + " berjumlah " + map.get(angka));
//            } else {
//                System.out.println("Nilai Pembatas adalah\t" + delimiter + ". Nilai " + angka + " berjumlah " + map.get(angka));
//            }
//        }
//
//    }
}
